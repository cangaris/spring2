gdrive: `https://drive.google.com/drive/folders/14NkRhVtssPUdyThVbVjpbGUT_SYHRIcq` <br>
link do repo: `https://gitlab.com/cangaris/spring2`

---

repozytorium maven: `https://mvnrepository.com/`

---

Lombok instalacja maven: `https://projectlombok.org/setup/maven` <br>
Lombok jest to dev tool do minimalizowania kodu technicznego i możliwości skupienia się na kodzie biznesowym

---

JACKSON - odpowiada za serializacje danych (tj. zamiana klas Javy na JSON i odwrotnie)
Wykorzytuje publiczne gettery i settery (nie pola, one są prywatne)

---

Zadanie 1:
1. Utworzyć ProductController i zrobić CRUDa (5 endpointów)
2. Utworzyć model Product z polam (id, name, price) używając lombok
3. Utworzyć wyjątek gdy produkt nie odnaleziony - ProductNotFoundException

---

Zadanie 2:
1. Utworzyć ProductService
2. Przenieść logikę biznesową do ProductService z ProductController
3. ProductController ma tylko uruchamiać metody z ProductService

---

Link do instalacji swagger: `https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api#1-spring-boot-dependency` <br>
Link do swaggera w naszej aplikacji: `http://localhost:8085/swagger-ui/` 

---

Spring Validation: `https://www.baeldung.com/spring-boot-bean-validation` <br>
`https://www.baeldung.com/java-bean-validation-not-null-empty-blank` <br>
Odpowiada za dostarczenie modułu walidacji do naszych modeli

---

Link do strony z regexpami: `https://regexr.com/` <br>

---

Zadanie 3:
1. Utworzyć model Category (id, name) wraz z walidacją
2. Utworzyć CategoryNotFoundException
3. Utworzyć CategoryController i CRUDa
4. Utworzyć CategoryService jako interface oraz CategoryServiceArrayList jako implementacja na wzór ProductService

---

TODO: Przygotować zadania z gwiazdką (trudniejsze dla chętnych do poszukania w google etc).

---

`https://restfulapi.net/http-methods/` - metody HTTP (różnice między GET, POST, PUT, PATCH, DELETE) <br>
Spring Data: `https://spring.io/guides/gs/accessing-data-mysql/`

Spring Data posiada w sobie Hibermate (framework zgodny ze standardem JPA)

RestController - obsługa zapytań HTTP
Service - logika biznesowa
Entity - służy do tworzenia tabel w bazie
Repository - kontakt z Hibernate (pod spodem z np. MySQL)

---

Zadanie 4:
- Utworzyć CategoryRepository (na wzór UserRepository)
- Utworzyć nową implementację serwisu CategoryService o nazwie CategoryServiceDatabase
- do CategoryServiceDatabase wstrzyknąć CategoryRepository
- w CategoryServiceDatabase wykonać zapytania np. findAll

---

Typy relacji:
1. one to one (o2o) @OneToOne
2. o2m / m2o
    a. one to many (o2m) @OneToMany
    b. many to one (m2o) @ManyToOne
3. many to many (m2m) @ManyToMany

---

app frontend -> json -> deserializacja (jackson) -> obiekty java (POJO)
-> Hibernate (ORM) (tłumaczy na SQL) -> mysql -> zapis -> obiekt mysql wraca po zapisie
-> Hibernate (ORM) (tłumaczy obiekty DB na POJO) -> serializacja (jasckon) -> json -> app front

---

Zadanie 5:
1. Utworzyć encję Tag, która będzie związana relacją m2m z Product
2. Utworzyć encję Color, która będzie związana m2m z Product
3. Utworzyć encję Image, która będzie:
    a. związana z Product relacją o2m (produkt ma wiele zdjęć)
    b. związana z Category relacją o2o (kategoria ma 1 zdjęcie)

---

Spring MVC: `https://spring.io/guides/gs/serving-web-content/` <br>
Thymeleaf Dokumentacja: `https://www.thymeleaf.org/documentation.html` <br>
Thymeleaf EACH: `https://www.baeldung.com/thymeleaf-iteration` <br>

---

Zadanie 6:
1. Utworzyć klasę Student
2. Utworzyć klasę Nauczyciel
3. Nauczyciel ma mieć pola: imię, nazwisko, wiek, email, przedmiot
4. Student ma mieć pola imię, nazwisko, wiek, email, kierunek (*dla chętnych - Nauczyciel oraz Student powinny mieć wspólnego przodka po którym mogą dziedziczyć - należy dowiedzieć się (zgoglować) jak utworzyć encję wykorzystując pola rodzica)
5. Obie klasy mają pełnego CRUDa (utworzyć obiekty kontrolera, repozytorium oraz serwisu)
6. Dane powinny być walidowane: poprawny email, imię dłuższe od 2 liter, wiek > 18
7. Powinna być możliwość wyświetlania wszystkich studentów danego nauczyciela (jako lista dla wszystkich, dla chętnych możliwość stronicowania i sortowania*)
8. Powinna być możliwość wyświetlania wszystkich nauczycieli danego studenta (jako lista dla wszystkich, dla chętnych możliwość stronicowania i sortowania*)
9. Studentów oraz nauczycieli można wyszukiwać po imieniu i nazwisku*

---

Pobieranie danych z adresu URL za pomocą "query string (parametry np. wyszukiwarki)": `https://www.baeldung.com/spring-request-param` <br>
Paginowanie za pomocą Pageable oraz Page: `https://www.baeldung.com/spring-data-jpa-pagination-sorting` <br>
Własne zapytania JPA (@Query): `https://www.baeldung.com/spring-data-jpa-query` <br>

---

Spring Security konfiguracja: `https://spring.io/guides/gs/securing-web/` <br>

---

Jackson JSON mapper: `https://www.baeldung.com/jackson-object-mapper-tutorial` <br>
Integration Tests: `https://www.baeldung.com/spring-boot-testing` <br>
JUnit - Unit Tests: `https://www.baeldung.com/junit-5` <br>
Mockito (mockowanie zależności w Spring): `https://www.baeldung.com/mockito-junit-5-extension` <br>

---

Zadanie 7: <br>
Wykorzystując Spring MVC należy utworzyć:<br>
1. Projekt, będący wyszukiwarką filmów
2. Posiadający na górze wyszukiwarkę, pod spodem listę filmów w formie kalefek z obrazkiem
3. W film można kliknąć i podejrzeć szczegóły (opis, tytuł, zdjęcia, kategorie, rok wydania, reżyser)
4. Film posiada kategorie (np. Komedia, Dramat etc)
5. Wyszukiwać można po nazwie filmu niezależnie czy słowo pokrywa się w 100% czy może występuje na początku, w środku czy na końcu tytułu
6. Jest możliwość filtrowania filmów po kategorii (obok wyszukiwarki, select zawężający wyszukiwania tylko do wybranej kategorii)
7. Możliwe jest wyszukiwanie również po reżyserze oraz roku wydania filmu (wymagana będzie możliwość zmiany kontekstu wyszukiwania z TYTUŁÓW właśnie na REŻYSERA lub ROK WYDANIA)
8. Po odnalezieniu film można dodać do ulubionych po zalogowaniu (panel usera z listą filmów dodanych do ulubionych)
9. Wszystkie listy powinny być pageowane (po 50 elementów), powinny posiadać strzałeczki do poprzedniej i następnej strony, informacje na której stronie się znajdujemy oraz ile stron zostało do końca
10. Wymagane jest dodanie również panelu admina celem dodawania nowych filmów, usuwania/edycji już dodanych
11. Należy otestować projekt aby coverage był minimum 75%
12. Baza danych - MySQL

