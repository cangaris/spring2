package pl.cansoft.spring2.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
        code = HttpStatus.CONFLICT,
        reason = "Użytkownik już istnieje!"
)
public class UserAlreadyExistsException extends RuntimeException {
}
