package pl.cansoft.spring2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring2.models.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
