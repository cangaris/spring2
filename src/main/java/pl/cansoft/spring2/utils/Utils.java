package pl.cansoft.spring2.utils;

import org.springframework.ui.Model;

import java.security.Principal;

public class Utils {

    public static void setLoggedUser(Model model, Principal principal) {
        var isLogged = principal != null;
        model.addAttribute("email", isLogged ? principal.getName() : "");
        model.addAttribute("isLogged", isLogged);
    }
}
