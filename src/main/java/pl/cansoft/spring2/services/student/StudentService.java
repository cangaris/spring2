package pl.cansoft.spring2.services.student;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.cansoft.spring2.models.Student;

import java.util.List;

public interface StudentService {

    List<Student> getStudents();

    Student createStudent(Student student);

    Student updateStudent(Long id, Student toSave);

    Student delStudent(Long id);

    Student getStudent(Long id);

    Page<Student> getStudentsByTeacher(Long teacherId, Pageable pageable);

    Page<Student> getStudentsBySearchEngine(String firstName, String lastName, Pageable pageable);
}
