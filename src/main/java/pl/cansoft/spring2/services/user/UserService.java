package pl.cansoft.spring2.services.user;

import pl.cansoft.spring2.models.User;

import java.util.List;

public interface UserService {

    List<User> getUsers();

    User createUser(User user);

    User updateUser(Long id, User userToSave);

    User delUser(Long id);

    User getUser(Long id);

    User getUser(String email);
}
