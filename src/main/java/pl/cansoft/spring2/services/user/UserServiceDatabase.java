package pl.cansoft.spring2.services.user;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.UserAlreadyExistsException;
import pl.cansoft.spring2.exceptions.UserNotFoundException;
import pl.cansoft.spring2.models.User;
import pl.cansoft.spring2.repositories.UserRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
@Primary
public class UserServiceDatabase implements UserService {

    final UserRepository userRepository;

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    /**
     * jeśli user ma nullowe id, wykona się "insert into"
     * jeśli user ma NIE nullowe id, wykona się "update"
     */
    @Override
    public User createUser(User user) {
        userRepository.findByEmail(user.getEmail())
                .ifPresent(foundUser -> { throw new UserAlreadyExistsException(); });
        var encoder = new BCryptPasswordEncoder();
        user.setId(null);
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User updateUser(Long id, User userToSave) {
        userToSave.setId(id);
        return userRepository.save(userToSave);
    }

    @Override
    public User delUser(Long id) {
        var user = getUser(id);
        userRepository.deleteById(id);
        return user;
    }

    @Override
    public User getUser(Long id) {
        return userRepository
                .findById(id)
                .orElseThrow(() -> new UserNotFoundException());
    }

    @Override
    public User getUser(String email) {
        return userRepository
                .findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
    }
}
