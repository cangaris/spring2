package pl.cansoft.spring2.services.user;

import org.springframework.stereotype.Service;
import pl.cansoft.spring2.models.User;

import java.util.List;

@Service
public class UserServiceNullData implements UserService {

    @Override
    public List<User> getUsers() {
        return null;
    }

    @Override
    public User createUser(User user) {
        return null;
    }

    @Override
    public User updateUser(Long id, User userToSave) {
        return null;
    }

    @Override
    public User delUser(Long id) {
        return null;
    }

    @Override
    public User getUser(Long id) {
        return null;
    }

    @Override
    public User getUser(String email) {
        return null;
    }
}
