package pl.cansoft.spring2.services.user;

import org.springframework.stereotype.Service;
import pl.cansoft.spring2.models.User;

import java.util.List;

@Service
public class UserServiceStaticData implements UserService {

    User userDb = new User();

    @Override
    public List<User> getUsers() {
        return List.of(userDb);
    }

    @Override
    public User createUser(User user) {
        return userDb;
    }

    @Override
    public User updateUser(Long id, User userToSave) {
        return userDb;
    }

    @Override
    public User delUser(Long id) {
        return userDb;
    }

    @Override
    public User getUser(Long id) {
        return userDb;
    }

    @Override
    public User getUser(String email) {
        return userDb;
    }
}
