package pl.cansoft.spring2.services.user;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.UserNotFoundException;
import pl.cansoft.spring2.models.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceDynamicData implements UserService {

    private List<User> users = new ArrayList<>();
    private Long nextId = 0L;

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public User createUser(User user) {
        user.setId(getNextId());
        users.add(user);
        return user;
    }

    @Override
    public User updateUser(Long id, User userToSave) {
        var userDb = findUser(id);
        userToSave.setId(userDb.getId());
        users.remove(userDb);
        users.add(userToSave);
        return userToSave;
    }

    @Override
    public User delUser(Long id) {
        var user = findUser(id);
        users.remove(user);
        return user;
    }

    @Override
    public User getUser(Long id) {
        return findUser(id);
    }

    @Override
    public User getUser(String email) {
        return null;
    }

    private Long getNextId() {
        return nextId += 1; // nextId = nextId + 1 to samo co nextId += 1
    }

    private User findUser (Long id) {
        return users.stream()
                .filter(user -> user.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException());
    }
}
