package pl.cansoft.spring2.services.category;

import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.CategoryNotFoundException;
import pl.cansoft.spring2.models.Category;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceArrayList implements CategoryService {

    private List<Category> categories = new ArrayList<>();
    private Long nextId = 0L;

    @Override
    public List<Category> getCategories() {
        return categories;
    }

    @Override
    public Category createCategory(Category category) {
        category.setId(getNextId());
        categories.add(category);
        return category;
    }

    @Override
    public Category updateCategory(Long id, Category categoryToSave) {
        var productDb = findCategory(id);
        categoryToSave.setId(productDb.getId());
        categories.remove(productDb);
        categories.add(categoryToSave);
        return categoryToSave;
    }

    @Override
    public Category delCategory(Long id) {
        var category = findCategory(id);
        categories.remove(category);
        return category;
    }

    @Override
    public Category getCategory(Long id) {
        return findCategory(id);
    }

    private Category findCategory(Long id) {
        return categories.stream()
                .filter(category -> category.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new CategoryNotFoundException());
    }

    private Long getNextId() {
        return nextId += 1;
    }
}
