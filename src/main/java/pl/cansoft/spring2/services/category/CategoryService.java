package pl.cansoft.spring2.services.category;

import pl.cansoft.spring2.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getCategories();

    Category createCategory(Category category);

    Category updateCategory(Long id, Category categoryToSave);

    Category delCategory(Long id);

    Category getCategory(Long id);
}
