package pl.cansoft.spring2.services.category;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.CategoryNotFoundException;
import pl.cansoft.spring2.exceptions.UserNotFoundException;
import pl.cansoft.spring2.models.Category;
import pl.cansoft.spring2.models.User;
import pl.cansoft.spring2.repositories.CategoryRepository;
import pl.cansoft.spring2.services.user.UserService;

import java.util.List;

@RequiredArgsConstructor
@Service
@Primary
public class CategoryServiceDatabase implements CategoryService {

    final CategoryRepository categoryRepository;

    @Override
    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category createCategory(Category category) {
        category.setId(null);
        return categoryRepository.save(category);
    }

    @Override
    public Category updateCategory(Long id, Category categoryToSave) {
        categoryToSave.setId(id);
        return categoryRepository.save(categoryToSave);
    }

    @Override
    public Category delCategory(Long id) {
        var user = getCategory(id);
        categoryRepository.deleteById(id);
        return user;
    }

    @Override
    public Category getCategory(Long id) {
        return categoryRepository
                .findById(id)
                .orElseThrow(() -> new CategoryNotFoundException());
    }
}
