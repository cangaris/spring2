package pl.cansoft.spring2.services.product;

import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.ProductNotFoundException;
import pl.cansoft.spring2.models.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceArrayList implements ProductService {

    private List<Product> products = new ArrayList<>();
    private Long nextId = 0L;

    @Override
    public List<Product> getProducts() {
        return products;
    }

    @Override
    public Product createProduct(Product product) {
        product.setId(getNextId());
        products.add(product);
        return product;
    }

    @Override
    public Product updateProduct(Long id, Product productToSave) {
        var productDb = findProduct(id);
        productToSave.setId(productDb.getId());
        products.remove(productDb);
        products.add(productToSave);
        return productToSave;
    }

    @Override
    public Product delProduct(Long id) {
        var product = findProduct(id);
        products.remove(product);
        return product;
    }

    @Override
    public Product getProduct(Long id) {
        return findProduct(id);
    }

    private Product findProduct(Long id) {
        return products.stream()
                .filter(product -> product.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new ProductNotFoundException());
    }

    private Long getNextId() {
        return nextId += 1;
    }
}
