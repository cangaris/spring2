package pl.cansoft.spring2.services.product;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.ProductNotFoundException;
import pl.cansoft.spring2.exceptions.UserNotFoundException;
import pl.cansoft.spring2.models.Product;
import pl.cansoft.spring2.models.User;
import pl.cansoft.spring2.repositories.ProductRepository;
import pl.cansoft.spring2.repositories.UserRepository;
import pl.cansoft.spring2.services.user.UserService;

import java.util.List;

@RequiredArgsConstructor
@Service
@Primary
public class ProductServiceDatabase implements ProductService {

    final ProductRepository productRepository;

    @Override
    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product createProduct(Product product) {
        product.setId(null);
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(Long id, Product productToSave) {
        productToSave.setId(id);
        return productRepository.save(productToSave);
    }

    @Override
    public Product delProduct(Long id) {
        var user = getProduct(id);
        productRepository.deleteById(id);
        return user;
    }

    @Override
    public Product getProduct(Long id) {
        return productRepository
                .findById(id)
                .orElseThrow(() -> new ProductNotFoundException());
    }
}
