package pl.cansoft.spring2.services.product;

import pl.cansoft.spring2.models.Product;

import java.util.List;

public interface ProductService {

    List<Product> getProducts();

    Product createProduct(Product product);

    Product updateProduct(Long id, Product productToSave);

    Product delProduct(Long id);

    Product getProduct(Long id);
}
