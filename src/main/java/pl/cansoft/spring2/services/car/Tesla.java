package pl.cansoft.spring2.services.car;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class Tesla implements Car {
    @Override
    public void sayHello() {
        System.out.println("Jestem Tesla");
    }
}
