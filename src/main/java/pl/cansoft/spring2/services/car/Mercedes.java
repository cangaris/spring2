package pl.cansoft.spring2.services.car;

import org.springframework.stereotype.Service;

@Service
public class Mercedes implements Car {
    @Override
    public void sayHello() {
        System.out.println("Jestem Mercedes");
    }
}
