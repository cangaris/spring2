package pl.cansoft.spring2.services.car;

import org.springframework.stereotype.Service;

@Service
public class Audi implements Car {
    @Override
    public void sayHello() {
        System.out.println("Jestem Audi");
    }
}
