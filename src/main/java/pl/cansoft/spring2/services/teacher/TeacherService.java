package pl.cansoft.spring2.services.teacher;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.cansoft.spring2.models.Teacher;

import java.util.List;

public interface TeacherService {

    List<Teacher> getTeachers();

    Teacher createTeacher(Teacher teacher);

    Teacher updateTeacher(Long id, Teacher toSave);

    Teacher delTeacher(Long id);

    Teacher getTeacher(Long id);

    Page<Teacher> getTeachersByStudent(Long studentId, Pageable pageable);

    Page<Teacher> getTeachersBySearchEngine(String firstName, String lastName, Pageable pageable);
}
