package pl.cansoft.spring2.services.teacher;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.UserNotFoundException;
import pl.cansoft.spring2.models.Teacher;
import pl.cansoft.spring2.repositories.TeacherRepository;
import pl.cansoft.spring2.services.student.StudentService;

import java.util.List;

/**
 * onConstructor = @__(@Lazy) - pozwala przekopiować adnotację @Lazy z pola do konstruktora za pomocą lomboka
 */
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
@Service
public class TeacherServiceImpl implements TeacherService {

    final TeacherRepository teacherRepository;
    @Lazy
    final StudentService studentService;

    @Override
    public List<Teacher> getTeachers() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher createTeacher(Teacher teacher) {
        teacher.setId(null);
        return teacherRepository.save(teacher);
    }

    @Override
    public Teacher updateTeacher(Long id, Teacher toSave) {
        toSave.setId(id);
        return teacherRepository.save(toSave);
    }

    @Override
    public Teacher delTeacher(Long id) {
        var teacher = getTeacher(id);
        teacherRepository.deleteById(id);
        return teacher;
    }

    @Override
    public Teacher getTeacher(Long id) {
        return teacherRepository.findById(id)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public Page<Teacher> getTeachersByStudent(Long studentId, Pageable pageable) {
        return teacherRepository.findAllByStudentId(studentId, pageable);
    }

    @Override
    public Page<Teacher> getTeachersBySearchEngine(String firstName, String lastName, Pageable pageable) {
        if (firstName != null && lastName != null) {
            return teacherRepository.findAllByFirstNameAndLastName(firstName, lastName, pageable);
        }
        if (firstName != null) {
            return teacherRepository.findAllByFirstName(firstName, pageable);
        }
        if (lastName != null) {
            return teacherRepository.findAllByLastName(lastName, pageable);
        }
        return teacherRepository.findAll(pageable);
    }
}
