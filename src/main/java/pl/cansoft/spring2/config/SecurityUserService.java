package pl.cansoft.spring2.config;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.cansoft.spring2.exceptions.UserNotFoundException;
import pl.cansoft.spring2.services.user.UserService;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class SecurityUserService implements UserDetailsService {

    final UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            var user = userService.getUser(email);
            return User.withUsername(user.getEmail())
                    .password(user.getPassword())
                    .authorities(user.getRoles())
                    .build();
        } catch (UserNotFoundException exception) {
            throw new UsernameNotFoundException(exception.getMessage());
        }
    }
}
