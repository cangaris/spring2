package pl.cansoft.spring2.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true) // włączenie adnotacji @Secured( { "ROLE_ADMIN" } )
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    final SecurityUserService securityUserService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // wyłączenie ochrony przed atakiem CSRF
                .csrf().disable()
                // włączenie autoryzacji dla wszystkich requestów
                .authorizeRequests()
                .antMatchers("/", "/category/{categoryId}", "/product/{productId}").permitAll()
                .antMatchers( "/api/user").permitAll()
                .antMatchers( "/api/user").permitAll()
                .antMatchers( "/api/user/{id}").permitAll()
                .anyRequest().authenticated()
                // dodanie strony logowania dostępnej dla wszystkich
                .and()
                .formLogin().loginPage("/login")
                .permitAll()
                // dodanie strony wylogowywania dostępnej dla wszystkich
                .and()
                .logout()
                .permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(securityUserService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }
}
