package pl.cansoft.spring2.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    /**
     * stategia 1 klucz na wszystkie tabele
     * - user [1,2,3]
     * - product [4,5]
     * - category [6]
     *
     * stategia 1 klucz na jedną tabele:
     * - user [1,2,3,4]
     * - product [1,2,3,4]
     * - category [1,2,3,4]
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 511, message = "Adres url obrazka musi mieć od 5 do 511 znaków")
    private String image;

    @Pattern(regexp = "^[A-Z][a-z]+$", message = "Imię musi składać się wykłącznie z liter a pierwsza litera musi być duża")
    @Size(min = 3, max = 30, message = "Imię musi mieć od 3 do 30 znaków")
    @NotBlank(message = "Imię jest wymagane!")
    private String firstName;

    @Pattern(regexp = "^[A-Z][a-z]+$", message = "Nazwisko musi składać się wykłącznie z liter a pierwsza litera musi być duża")
    @Size(min = 5, max = 50, message = "Nazwisko musi mieć od 5 do 50 znaków")
    @NotBlank(message = "Nazwisko jest wymagane!")
    private String lastName;

    @Min(value = 18, message = "Minimalny wiek to 18 lat")
    @Max(value = 120, message = "Maksymalny wiek to 120 lat")
    @NotNull(message = "Podanie wieku jest wymagane!")
    private Byte age;

    @Min(value = 3000, message = "Minimalne wynagrodzenie to 3 000 zł")
    @Max(value = 50_000, message = "Maksymalne wynagrodzenie to 50 000 zł")
    @NotNull(message = "Podanie wynagrodzenia jest wymagane!")
    private Integer salary;

    @Size(max = 255, message = "Nr karty kredytowej musi mieć od 5 do 255 znaków")
    private String creditCardNo;

    @Size(min = 5, max = 50, message = "Email musi mieć od 5 do 50 znaków")
    @Email(message = "Email jest niepoprawny")
    @NotBlank(message = "Email jest wymagany")
    @Column(unique = true)
    private String email;

    @Size(min = 5, max = 255, message = "Hasło musi mieć od 5 do 255 znaków")
    @NotBlank(message = "Hasło jest wymagane")
    private String password;

    private LocalDateTime createdAt;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Role> roles;

    public String getRolesAsString() {
        return roles == null ? "" : roles.stream()
                .map(role -> role.getName())
                .collect(Collectors.joining(", "));
    }

    @PrePersist
    private void prePersist() {
        createdAt = LocalDateTime.now();
    }
}
