package pl.cansoft.spring2.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Pattern(regexp = "^[a-zA-Z0-9 ]+$", message = "Nazwa musi składać się wyłącznie z liter lub cyfer lub spacji")
    @Size(min = 3, max = 100, message = "Nazwa musi mieć od 3 do 100 znaków")
    @NotBlank(message = "Nazwa jest wymagana!")
    private String name;

    @NotNull(message = "Cena jest wymagana!")
    @DecimalMin(value = "1.00", message = "Minimalna cena to 1.00")
    @DecimalMax(value = "100000.00", message = "Maksymalna cena to 100000.00")
    private BigDecimal price;

    @JsonBackReference
    @ManyToOne
    private Category category;

    @OneToOne
    private ProductDetails details;

    @OneToMany(mappedBy = "product")
    private List<Image> images;

    @ManyToMany
    private List<Tag> tags;

    @ManyToMany
    private List<Color> colors;
}
