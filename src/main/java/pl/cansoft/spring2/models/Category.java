package pl.cansoft.spring2.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Pattern(regexp = "^[a-zA-Z0-9 ]+$", message = "Nazwa musi składać się wyłącznie z liter lub cyfer lub spacji")
    @Size(min = 3, max = 100, message = "Nazwa musi mieć od 3 do 100 znaków")
    @NotBlank(message = "Nazwa jest wymagana!")
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy = "category")
    private List<Product> products;

    @OneToOne
    private Image image;
}
