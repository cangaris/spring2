package pl.cansoft.spring2.controllers.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.spring2.models.User;
import pl.cansoft.spring2.services.user.UserService;

import javax.validation.Valid;
import java.util.List;

/**
 * CRUD - Create, Read, Update, Delete
 * REST Standard
 * (+) GET /user -> [User]
 * (+) GET /user/1 -> User id 1
 * (+) POST /user -> create new user
 * (+) PUT /user/1 -> update user
 * (+) PATCH /user/1 -> update user
 * (+) DELETE /user/1 -> delete user
 */
@CrossOrigin("*")
@RestController
@RequiredArgsConstructor
@RequestMapping("api/user")
public class UserRestController {

    /**
     * DI - dependency injection (wstrzykiwanie zależności)
     * IoC - inversion of control (odwrócona kontrola)
     * Application Context - podczas uruchamiania skanowane są beany, tworzone obiekty i umieszczane w kontenerze
     */
    final UserService userService;

    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("{id}")
    public User getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PostMapping
    public User createUser(@Valid @RequestBody User user) {
        return userService.createUser(user);
    }

    // @RequestMapping(method = {RequestMethod.PUT, RequestMethod.PATCH}, value = "{id}")
    @PutMapping("{id}") // to samo co -> @PutMapping(value = "{id}")
    public User updateUser(@PathVariable Long id, @Valid @RequestBody User userToSave) {
        return userService.updateUser(id, userToSave);
    }

    @DeleteMapping("{id}")
    public User delUser(@PathVariable Long id) {
        return userService.delUser(id);
    }
}
