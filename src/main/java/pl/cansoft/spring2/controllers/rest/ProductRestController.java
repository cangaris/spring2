package pl.cansoft.spring2.controllers.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.spring2.models.Product;
import pl.cansoft.spring2.services.product.ProductService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/product")
public class ProductRestController {

    final ProductService productService;

    @GetMapping
    public List<Product> getProducts() {
        return productService.getProducts();
    }

    @GetMapping("{id}")
    public Product getProduct(@PathVariable Long id) {
        return productService.getProduct(id);
    }

    @PostMapping
    public Product createProduct(@Valid @RequestBody Product product) {
        return productService.createProduct(product);
    }

    @PutMapping("{id}")
    public Product updateProduct(@PathVariable Long id, @Valid @RequestBody Product productToSave) {
        return productService.updateProduct(id, productToSave);
    }

    @DeleteMapping("{id}")
    public Product delProduct(@PathVariable Long id) {
        return productService.delProduct(id);
    }
}
