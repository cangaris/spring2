package pl.cansoft.spring2.controllers.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.spring2.models.Category;
import pl.cansoft.spring2.services.category.CategoryService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/category")
public class CategoryRestController {

    final CategoryService categoryService;

    @GetMapping
    public List<Category> getCategories() {
        return categoryService.getCategories();
    }

    @GetMapping("{id}")
    public Category getCategory(@PathVariable Long id) {
        return categoryService.getCategory(id);
    }

    @PostMapping
    public Category createCategory(@Valid @RequestBody Category category) {
        return categoryService.createCategory(category);
    }

    @PutMapping("{id}")
    public Category updateCategory(@PathVariable Long id, @Valid @RequestBody Category categoryToSave) {
        return categoryService.updateCategory(id, categoryToSave);
    }

    @DeleteMapping("{id}")
    public Category delCategory(@PathVariable Long id) {
        return categoryService.delCategory(id);
    }
}
