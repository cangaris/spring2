package pl.cansoft.spring2.controllers.mvc;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.cansoft.spring2.services.product.ProductService;
import pl.cansoft.spring2.utils.Utils;

import java.security.Principal;

/**
 * MVC - model, view, controller
 */
@RequiredArgsConstructor
@Controller
public class ProductController {

    final ProductService productService;

    @GetMapping("product/{productId}")
    public String showProductById(@PathVariable Long productId, Model model, Principal principal) {
        Utils.setLoggedUser(model, principal);
        var product = productService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }
}
