package pl.cansoft.spring2.controllers.mvc;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.cansoft.spring2.services.category.CategoryService;
import pl.cansoft.spring2.utils.Utils;

import java.security.Principal;

/**
 * MVC - model, view, controller
 */
@RequiredArgsConstructor
@Controller
public class CategoryController {

    final CategoryService categoryService;

    @GetMapping
    public String showCategories(Model model, Principal principal) {
        Utils.setLoggedUser(model, principal);
        var categories = categoryService.getCategories();
        model.addAttribute("categories", categories);
        return "categories";
    }

    @GetMapping("category/{categoryId}")
    public String showCategoryById(@PathVariable Long categoryId, Model model, Principal principal) {
        Utils.setLoggedUser(model, principal);
        var category = categoryService.getCategory(categoryId);
        model.addAttribute("category", category);
        return "category";
    }
}
