package pl.cansoft.spring2.controllers.mvc;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.cansoft.spring2.services.user.UserService;
import pl.cansoft.spring2.utils.Utils;

import java.security.Principal;

/**
 * MVC - model, view, controller
 */
@RequiredArgsConstructor
@Controller
public class UserPanelController {

    final UserService userService;

    @Secured("ROLE_USER")
    @GetMapping("panel/user")
    public String showProductById(Model model, Principal principal) {
        Utils.setLoggedUser(model, principal);
        var user = userService.getUser(principal.getName());
        model.addAttribute("user", user);
        return "userDetails";
    }
}
