package pl.cansoft.spring2.services.utils;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import pl.cansoft.spring2.models.Role;
import pl.cansoft.spring2.models.User;

import java.util.List;

public class UserTest {

    @Test
    void testRolesAsStringGivenEmptyList() {
        // given
        var user = new User();
        user.setRoles(List.of());
        // when
        var str = user.getRolesAsString();
        // then
        assertEquals("", str);
    }

    @Test
    void testRolesAsStringGivenOneItem() {
        // given
        var user = new User();
        var role = new Role();
        role.setName("ROLE_ADMIN");
        user.setRoles(List.of(role));
        // when
        var str = user.getRolesAsString();
        // then
        assertEquals("ROLE_ADMIN", str);
    }

    @Test
    void testRolesAsStringGivenTwoItems() {
        // given
        var user = new User();
        var role1 = new Role();
        role1.setName("ROLE_ADMIN");
        var role2 = new Role();
        role2.setName("ROLE_USER");
        user.setRoles(List.of(role1, role2));
        // when
        var str = user.getRolesAsString();
        // then
        assertEquals("ROLE_ADMIN, ROLE_USER", str);
    }

    @Test
    void testRolesAsStringGivenNull() {
        // given
        var user = new User();
        user.setRoles(null);
        // when
        var str = user.getRolesAsString();
        // then
        assertEquals("", str);
    }
}
