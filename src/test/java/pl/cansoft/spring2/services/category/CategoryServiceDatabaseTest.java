package pl.cansoft.spring2.services.category;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.cansoft.spring2.exceptions.CategoryNotFoundException;
import pl.cansoft.spring2.models.Category;
import pl.cansoft.spring2.repositories.CategoryRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CategoryServiceDatabaseTest {

    @InjectMocks
    CategoryServiceDatabase categoryServiceDatabase;

    @Mock
    CategoryRepository categoryRepository;

    @BeforeEach // przed każdym testem
    void setUp() {
        // todo: nie potrzebne ale zostawiam dla celów edukacyjnych
    }

    @AfterEach // po każdym testem
    void tearDown() {
        // todo: nie potrzebne ale zostawiam dla celów edukacyjnych
    }

    @Test
    void getCategories() {
        // given
        List<Category> categoriesIn = List.of(new Category());
        // when
        doReturn(categoriesIn).when(categoryRepository).findAll();
        var categoriesOut = categoryServiceDatabase.getCategories();
        // then
        assertEquals(1, categoriesOut.size());
        verify(categoryRepository, times(1)).findAll();
    }

    @Test
    void getCategory() {
        // given
        var categoryId = 1L;
        var categoryIn = new Category();
        categoryIn.setId(categoryId);
        // when
        Mockito.doReturn(Optional.of(categoryIn)).when(categoryRepository).findById(categoryId);
        var categoryOut = categoryServiceDatabase.getCategory(categoryId);
        // then
        assertEquals(categoryId, categoryOut.getId());
        Mockito.verify(categoryRepository, Mockito.times(1)).findById(categoryId);
    }

    @Test
    void getCategoryShouldThrowCategoryNotFoundException() {
        // given
        var categoryId = 1L;
        var categoryIn = new Category();
        categoryIn.setId(categoryId);
        // when
        Mockito.doReturn(Optional.empty()).when(categoryRepository).findById(categoryId);
        // then
        assertThrows(CategoryNotFoundException.class, () -> categoryServiceDatabase.getCategory(categoryId));
    }

    @Test
    void createCategory() {
        // given
        var name = "Damian";
        var categoryIn = new Category();
        categoryIn.setName(name);
        // when
        Mockito.doReturn(categoryIn).when(categoryRepository).save(categoryIn);
        var categoryOut = categoryServiceDatabase.createCategory(categoryIn);
        // then
        assertEquals(name, categoryOut.getName());
        Mockito.verify(categoryRepository, Mockito.times(1)).save(categoryIn);
    }

    @Test
    void updateCategory() {
        // given
        var categoryId = 1L;
        var name = "Damian";
        var categoryIn = new Category();
        categoryIn.setName(name);
        // when
        Mockito.doReturn(categoryIn).when(categoryRepository).save(categoryIn);
        var categoryOut = categoryServiceDatabase.updateCategory(categoryId, categoryIn);
        // then
        assertEquals(name, categoryOut.getName());
        Mockito.verify(categoryRepository, Mockito.times(1)).save(categoryIn);
    }

    @Test
    void delCategory() {
        // given
        var categoryId = 1L;
        var categoryIn = new Category();
        categoryIn.setId(categoryId);
        // when
        Mockito.doNothing().when(categoryRepository).deleteById(categoryId);
        Mockito.doReturn(Optional.of(categoryIn)).when(categoryRepository).findById(categoryId);
        var categoryOut = categoryServiceDatabase.delCategory(categoryId);
        // then
        assertEquals(categoryId, categoryOut.getId());
        Mockito.verify(categoryRepository, Mockito.times(1)).deleteById(categoryId);
        Mockito.verify(categoryRepository, Mockito.times(1)).findById(categoryId);
    }
}
